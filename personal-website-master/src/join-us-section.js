const joinUsSection = (function () {
  function createStandardSection() {
    const container = document.getElementById("container");

    const textElement = document.createElement("h3");
    textElement.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    container.appendChild(textElement);

    const titleElement = document.createElement("h1");
    titleElement.textContent = "Join Our Program";
    container.appendChild(titleElement);


    const searchBar = document.createElement("div");
    searchBar.classList.add("search-framework");
    container.appendChild(searchBar);


    const joinForm = document.getElementById("joinForm");
    joinForm.addEventListener("SUBSCRIBE", function (event) {
      event.preventDefault();

      const enteredEmail = emailInput.value;
      // eslint-disable-next-line no-console
      console.log("Entered email:", enteredEmail);
    });

    const unusedVariable = 10;

    subscribeButton.addEventListener("click", function () {
      emailInput.value = "";
    });
  }

  function createAdvancedSection() {
    const container = document.getElementById("container");

    const titleElement = document.createElement("h1");
    titleElement.textContent = "Join Our Advanced Program";
    container.appendChild(titleElement);

    const textElement = document.createElement("h3");
    textElement.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    container.appendChild(textElement);

    const searchBar = document.createElement("div");
    searchBar.classList.add("search-framework");
    container.appendChild(searchBar);

    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.placeholder = "Email";
    emailInput.required = true;
    searchBar.appendChild(emailInput);

    const subscribeButton = document.createElement("button");
    subscribeButton.textContent = "Subscribe to Advanced Program";
    searchBar.appendChild(subscribeButton);

    const joinForm = document.getElementById("joinForm");
    joinForm.addEventListener("SUBSCRIBE", function (event) {
      event.preventDefault();

      const enteredEmail = emailInput.value;
      console.log("Entered email:", enteredEmail);
    });

    subscribeButton.addEventListener("click", function () {
      emailInput.value = "";
    });
  }

  class StandardSection {
    remove() {
      const container = document.getElementById("container");
      container.innerHTML = "";
    }
  }

  class AdvancedSection {
    remove() {
      const container = document.getElementById("container");
      container.innerHTML = "";
    }
  }

  class SectionCreator {
    create(type) {
      switch (type) {
        case "standard":
          return new StandardSection();
        case "advanced":
          return new AdvancedSection();
        default:
          throw new Error("Invalid section type");
      }
    }
  }


  createStandardSection();


  document.addEventListener("DOMContentLoaded", function () {
    const standardButton = document.getElementById("standardButton");
    const advancedButton = document.getElementById("advancedButton");

    standardButton.addEventListener("click", function () {
      createStandardSection();
    });

    advancedButton.addEventListener("click", function () {
      createAdvancedSection();
    });
  });

  return {
    createStandardSection: createStandardSection,
    createAdvancedSection: createAdvancedSection,
    sectionCreator: new SectionCreator(),
  };
})();

export default joinUsSection;
