import './styles/style.css';

import { validate } from './email-validator.js';

const emailInput = document.getElementById('email');

function handleSubmit(event) {
  event.preventDefault();

  const email = emailInput.value;

  const isValidEmail = validate(email);

  if (isValidEmail) {
    alert('Valid email address!');
  } else {
    alert('Invalid email address. Please use a valid email ending.');
  }
}

const form = document.getElementById('contactForm');
form.addEventListener('submit', handleSubmit);
